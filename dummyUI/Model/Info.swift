//
//  Info.swift
//  dummyUI
//
//  Created by Marco Lopes on 12/05/2018.
//  Copyright © 2018 Marco Lopes. All rights reserved.
//

import UIKit

// A very rude implementation of the data model

class SectionInfo: NSObject {
    
    var title: String?
    var cellInfo: [CellInfo]
    
    init(cellInfo: [CellInfo]) {
        self.cellInfo = cellInfo
    }
    
    init(title: String, cellInfo: [CellInfo]) {
        self.title = title
        self.cellInfo = cellInfo
    }
    
}

class CellInfo: NSObject {     //change this name please
    
    var name: String?
    var imageName: String?
    var tableOfItems: [Item]?
    
    override init() {
        super.init()
    }
    
    init(name: String) {
        self.name = name
    }
    
    init(name: String, imageName: String) {
        self.name = name
        self.imageName = imageName
    }
    
    init(name: String, imageName: String, tableOfItems: [Item]) {
        self.name = name
        self.imageName = imageName
        self.tableOfItems = tableOfItems
    }
}

class Item: NSObject {
    
    var name: String
    var listOfThings: [Thing]?
    
    init(name: String) {
        self.name = name
    }
    
    init(name: String, listOfThings: [Thing]?) {
        self.name = name
        if listOfThings != nil {
            self.listOfThings = listOfThings
        }
    }
}

class Thing: NSObject {
    var image: String?
    var thingDescription: String?
}
