//
//  TableViewController.swift
//  dummyUI
//
//  Created by Marco Lopes on 16/05/2018.
//  Copyright © 2018 Marco Lopes. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    var cellInfo: CellInfo? {
        didSet {
            navigationItem.title = cellInfo?.name
        }
    }
    
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationController?.navigationBar.tintColor = .white
        let textAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        setupNavigationBar()
    }
    
    func setupNavigationBar() {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        titleLabel.text = navigationItem.title
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        navigationItem.titleView = titleLabel
        
        //this back button is what appears in the next element of the stack
        let backItem = UIBarButtonItem()
        backItem.title = "    "
        navigationItem.backBarButtonItem = backItem
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellInfo?.tableOfItems?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let itemInfo = cellInfo?.tableOfItems?[indexPath.row] {
            
            let layout = UICollectionViewFlowLayout()
            let collectionViewController = CollectionViewController(collectionViewLayout: layout)
            collectionViewController.itemInfo = itemInfo
            navigationController?.pushViewController(collectionViewController, animated: true)
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.textLabel?.text = cellInfo?.tableOfItems?[indexPath.row].name
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
        cell.textLabel?.textColor = UIColor.rgb(red: 65, green: 65, blue: 65)
        cell.indentationLevel = 1
        
        return cell
    }
}
