//
//  CollectionViewController.swift
//  dummyUI
//
//  Created by Marco Lopes on 16/05/2018.
//  Copyright © 2018 Marco Lopes. All rights reserved.
//

import UIKit

class CollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var itemInfo: Item? {
        didSet {
            navigationItem.title = itemInfo?.name
        }
    }
    
    let descriptionHeight: CGFloat = 42
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = .white

        collectionView?.register(Cell.self, forCellWithReuseIdentifier: "cellId")
        
        setupNavigationBar()
    }
    
    func setupNavigationBar() {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        titleLabel.text = navigationItem.title
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        navigationItem.titleView = titleLabel
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let numOfItems = itemInfo?.listOfThings?.count ?? 0
        return numOfItems
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! Cell
        
        if let descriptionText = itemInfo?.listOfThings?[indexPath.item].thingDescription {
            cell.descriptionText.text = descriptionText
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.width / 2) - 26
        return CGSize(width: width, height: width + descriptionHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 17, left: 17, bottom: 17, right: 17)
    }
    
    class Cell: BaseCell {
        let descriptionText: UITextView = {
            let dt = UITextView()
            dt.text = "This is a long description!"
            dt.font = UIFont.systemFont(ofSize: 13)
            dt.textColor = UIColor.rgb(red: 85, green: 85, blue: 85)
            dt.isEditable = false
            return dt
        }()
        
        let imageView: UIImageView = {
            let iv = UIImageView()
            iv.backgroundColor = UIColor.rgb(red: 190, green: 200, blue: 200)
            iv.contentMode = .scaleAspectFill
            return iv
        }()
        
        override func setupViews() {
            addSubview(imageView)
            addSubview(descriptionText)
            
            addConstraints(withFormat: "H:|[v0]|", views: imageView)
            addConstraints(withFormat: "H:|[v0]|", views: descriptionText)
            //addConstraints(withFormat: "V:|[v0][v1\(descriptionHeight)]|", views: imageView, descriptionLabel)
            addConstraints(withFormat: "V:|[v0][v1(42)]|", views: imageView, descriptionText)
        }
    }
}


