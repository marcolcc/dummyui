//
//  ViewController.swift
//  dummyUI
//
//  Created by Marco Lopes on 11/05/2018.
//  Copyright © 2018 Marco Lopes. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

//    let cellId = "cellId"
    let menuBarHeight: CGFloat = 48
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    lazy var statusBarBackgroundView = self.appDelegate.statusBarBackgroundView
    
    let info: [SectionInfo] = {
        let dayCells: [CellInfo] = {
            var cells: [CellInfo] = []
            for i in 1...9 {
                
                var items = [Item]()
                for i in 1...50 {
                    let listNumber = String(format: "%02d", i)
                    let item = Item(name: "List \(listNumber)", listOfThings: [Thing]())
                    for j in 1...20 {
                        let thing = Thing()
                        let descriptionNumber = String(format: "%02d", j)
                        thing.thingDescription = "Description \(descriptionNumber)"
                        item.listOfThings?.append(thing)
                    }
                    items.append(item)
                }
                
                let cellName = "Day \(String(format: "%02d", i))"
                let imageName = "tech-\(i)"
                cells.append(CellInfo(name: cellName, imageName: imageName, tableOfItems: items))
            }
            return cells
        }()
        
        let noNameCells: [CellInfo] = {
            var cells: [CellInfo] = []
            for _ in 1...30 {
                cells.append(CellInfo())
            }
            return cells
        }()

        let info = [SectionInfo(title: "Open Day '18", cellInfo: dayCells),
                    SectionInfo(title: "Graduate Program", cellInfo: noNameCells),
                    SectionInfo(title: "Meet Mindera Code & Culture", cellInfo: noNameCells),
                    SectionInfo(title: "More stuff", cellInfo: noNameCells),
                    SectionInfo(title: "And even more stuff", cellInfo: noNameCells),
                    SectionInfo(title: "The End", cellInfo: noNameCells),]
        return info
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        refreshStatusBarIsHidden()
        view.backgroundColor = UIColor.rgb(red: 190, green: 200, blue: 200)
        setupBottomView()
        setupCollectionView()
        setupMenuBar()
        setupNavigationBar()
        
    }
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        //cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        return cv
    }()
    
    let menuBar: MenuBar = {
        let mb = MenuBar()
        return mb
    }()
    
    private func setupMenuBar() {
        
        view.addSubview(menuBar)
        view.addConstraints(withFormat: "H:|[v0]|", views: menuBar)
        view.addConstraints(withFormat: "V:|[v0(\(menuBarHeight))]", views: menuBar)
        
    }
    
    private func setupBottomView() {
        let bottomView = UIView()
        view.addSubview(bottomView)
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.backgroundColor = .white
        bottomView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        bottomView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        bottomView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottomView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
    
    private func setupCollectionView() {
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        view.addSubview(collectionView)
        
        for (index, _) in info.enumerated() {
            collectionView.register(OuterCell.self, forCellWithReuseIdentifier: cellId(withIndex: index))
        }

// alternative would be to make all the section cells with the same ID but then I would have to store all the offsets of all the sections and then reset them programatically.
//        collectionView.register(OuterCell.self, forCellWithReuseIdentifier: cellId)
        
        collectionView.contentInset.top = 220
        collectionView.scrollIndicatorInsets.top = menuBarHeight

        view.addConstraints(withFormat: "H:|[v0]|", views: collectionView)
        view.addConstraints(withFormat: "V:|[v0]|", views: collectionView)
    }
    
    
    private func cellId(withIndex index: Int) -> String {
        return String(info[index].hashValue)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return info.count 
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId(withIndex: indexPath.row), for: indexPath) as! OuterCell
        //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! OuterCell
        cell.sectionInfo = info[indexPath.row]
        cell.titleView.text = info[indexPath.row].title
        cell.mainViewController = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height: CGFloat = UIDevice.IS_IPHONE_5 ? 210 : 230
        
        return CGSize(width: view.frame.width, height: height)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        refreshStatusBarIsHidden()
        collectionView.reloadData()
        menuBar.menuCollectionView.reloadData()
    }
    
    func refreshStatusBarIsHidden() {
        if UIDevice.current.orientation.isLandscape {
            statusBarBackgroundView.isHidden = true
        } else {
            statusBarBackgroundView.isHidden = false
        }
    }
    
    func showNextNavigationItem(withCellInfo cellInfo: CellInfo) {
        let tableViewController = TableViewController()
        tableViewController.cellInfo = cellInfo
        
        navigationController?.pushViewController(tableViewController, animated: true)
    }
    
    private func setupNavigationBar() {
        
        //set the navigation bar to opaque
        navigationController?.navigationBar.isTranslucent = false
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        titleLabel.text = " Meet Mindera"
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        navigationItem.titleView = titleLabel
        
        let menuImage = UIImage(named: "Hamburger_icon")?.withRenderingMode(.alwaysOriginal)
        let menuBarButtonItem = UIBarButtonItem(image: menuImage, style: .plain, target: self, action: #selector(handleMenu))
        navigationItem.setLeftBarButton(menuBarButtonItem, animated: true)
        
        let searchImage = UIImage(named: "search")?.withRenderingMode(.alwaysOriginal)
        let searchBarButtonItem = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handleSearch))
        navigationItem.setRightBarButton(searchBarButtonItem, animated: true)
        
        //this back button is what appears in the next element of the stack
        let backItem = UIBarButtonItem()
        backItem.title = "    "
        navigationItem.backBarButtonItem = backItem
        
//        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "left-arrow")
//        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "left-arrow")
    }
    
    @objc func handleSearch() {
        showOkAlert(withTitle: "Search feature", message: "Not implemented.")
    }
    
    @objc func handleMenu() {
        showOkAlert(withTitle: "Menu feature", message: "Not implemented.")
    }
}

