//
//  AppDelegate.swift
//  dummyUI
//
//  Created by Marco Lopes on 11/05/2018.
//  Copyright © 2018 Marco Lopes. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let statusBarBackgroundView = UIView()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let mainViewController = MainViewController()
        let navigationController = UINavigationController(rootViewController: mainViewController) //navigationController.viewControllers = [mainViewController]
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = navigationController
        
        //set the navigation bar background color to grey
        UINavigationBar.appearance().barTintColor = UIColor.rgb(red: 131, green: 145, blue: 154)
        
        //set the application top bar info to white
        //for this to work, in Info.plist add "View controller-based status bar appearance" and set it to "NO"
        application.statusBarStyle = .lightContent
        
        //set the top bar color to a darker grey:
        //let statusBarBackgroundView = UIView() //I placed this as a global accessible reference so I can change it later inside other classes
        statusBarBackgroundView.backgroundColor = UIColor.rgb(red: 68, green: 76, blue: 79)
        statusBarBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        window?.addSubview(statusBarBackgroundView)
        NSLayoutConstraint.activate([statusBarBackgroundView.topAnchor.constraint(equalTo: window!.topAnchor),
                                     statusBarBackgroundView.leadingAnchor.constraint(equalTo: window!.leadingAnchor),
                                     statusBarBackgroundView.trailingAnchor.constraint(equalTo: window!.trailingAnchor)])
        
        if UIDevice.IS_IPHONE_X {
            statusBarBackgroundView.bottomAnchor.constraint(equalTo: window!.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            statusBarBackgroundView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        }
        
        //window?.addConstraints(withFormat: "H:|[v0]|", views: statusBarBackgroundView)
        //window?.addConstraints(withFormat: "V:|[v0(20)]", views: statusBarBackgroundView)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

