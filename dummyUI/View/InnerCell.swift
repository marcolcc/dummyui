//
//  innerCollectionViewCell.swift
//  dummyUI
//
//  Created by Marco Lopes on 12/05/2018.
//  Copyright © 2018 Marco Lopes. All rights reserved.
//

import UIKit

class InnerCell: BaseCell {
    
    let nameView: UILabel = {
        let nv = UILabel()
        nv.text = "text"
        nv.textAlignment = .center
        nv.textColor = UIColor.white
        return nv
    }()
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    override func setupViews() {
        
        addSubview(imageView)
        addConstraints(withFormat: "H:|[v0]|", views: imageView)
        addConstraints(withFormat: "V:|[v0]|", views: imageView)
        
        addSubview(nameView)
        addConstraints(withFormat: "H:|-8-[v0]-8-|", views: nameView)
        addConstraints(withFormat: "V:[v0]-8-|", views: nameView)
        
    }
}
