//
//  OuterCell.swift
//  dummyUI
//
//  Created by Marco Lopes on 12/05/2018.
//  Copyright © 2018 Marco Lopes. All rights reserved.
//

import UIKit

class OuterCell: BaseCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let innerCellBorder: CGFloat = 6
    let lateralInsetForSection: CGFloat = 10
    
    var mainViewController: MainViewController?
    
    var sectionInfo: SectionInfo? {
        didSet {
            //horizontalCollectionView.register(InnerCell.self, forCellWithReuseIdentifier: String((sectionInfo?.hashValue)!))
            horizontalCollectionView.reloadData()
        }
    }
    
    let cellId = "cellId"
    
    let titleView: UILabel = {
        let tv = UILabel()
        tv.text = "outer title text"
        tv.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        tv.textColor = UIColor.rgb(red: 65, green: 65, blue: 65)
        return tv
    }()
    
    lazy var horizontalCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = innerCellBorder * 2
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.clear
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    override func setupViews() {
        super.setupViews()
        
        backgroundColor = UIColor.white
        
        addSubview(titleView)
        addSubview(horizontalCollectionView)
        
        horizontalCollectionView.register(InnerCell.self, forCellWithReuseIdentifier: cellId) //String((sectionInfo?.hashValue)!))
        horizontalCollectionView.showsHorizontalScrollIndicator = false
        
        let leftSpacingForTitle = innerCellBorder + lateralInsetForSection
        //addConstraints(withFormat: "H:|-\(leftSpacingForTitle)-[v0]", views: titleView)
        titleView.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor, constant: leftSpacingForTitle).isActive = true
        titleView.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor).isActive = true
        
        horizontalCollectionView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        horizontalCollectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        //addConstraints(withFormat: "H:|[v0]|", views: horizontalCollectionView)
        addConstraints(withFormat: "V:|-14-[v0(20)][v1]|", views: titleView, horizontalCollectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (sectionInfo != nil) ? sectionInfo!.cellInfo.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! InnerCell//String((sectionInfo?.hashValue)!), for: indexPath) as! InnerCell
        if let cellInfo = sectionInfo?.cellInfo[indexPath.row] {
            cell.nameView.text = cellInfo.name
            
            if let imageName = cellInfo.imageName {
                cell.imageView.image = UIImage(named: imageName)
            } else {   // this else is not necessary in this implementation
                cell.imageView.image = nil
            }
            
            cell.backgroundColor = UIColor.rgb(red: 190, green: 200, blue: 200)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.height * 5/7, height: collectionView.frame.height - (innerCellBorder * 2))
        //return CGSize(width: 140, height: collectionView.frame.height - (innerCellBorder * 2) )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: lateralInsetForSection + innerCellBorder, bottom: 0, right: lateralInsetForSection + innerCellBorder)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cellInfo = sectionInfo?.cellInfo[indexPath.item] {
            mainViewController?.showNextNavigationItem(withCellInfo: cellInfo)
        }
    }
}
