//
//  MenuBar.swift
//  dummyUI
//
//  Created by Marco Lopes on 12/05/2018.
//  Copyright © 2018 Marco Lopes. All rights reserved.
//

import UIKit

class MenuBar: UIView, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    lazy var menuCollectionView: UICollectionView  = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.rgb(red: 131, green: 145, blue: 154)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let cellId = "cellId"
    let titleNames = ["Events", "Vacancies"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //because I am constructing a collection view inside a normal view I need to add it and make the necessary constraints
        addSubview(menuCollectionView)
        addConstraints(withFormat: "H:|[v0]|", views: menuCollectionView)
        addConstraints(withFormat: "V:|[v0]|", views: menuCollectionView)
        menuCollectionView.register(MenuBarCell.self, forCellWithReuseIdentifier: cellId)
        
        menuCollectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .left)
        
        setupHorizontaLine()
    }
    
    
    var horizontalLineLeftAnchorConstant: NSLayoutConstraint?
    
    func setupHorizontaLine() {
        let horizontalLine = UIView()
        horizontalLine.backgroundColor = UIColor.white
        horizontalLine.translatesAutoresizingMaskIntoConstraints = false
        addSubview(horizontalLine)
        
        horizontalLineLeftAnchorConstant = horizontalLine.leftAnchor.constraint(equalTo: menuCollectionView.leftAnchor)
        horizontalLineLeftAnchorConstant?.isActive = true
        horizontalLine.bottomAnchor.constraint(equalTo: menuCollectionView.bottomAnchor).isActive = true
        horizontalLine.widthAnchor.constraint(equalTo: menuCollectionView.widthAnchor, multiplier: 1/2)  .isActive = true
        horizontalLine.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MenuBarCell
        
        cell.cellTitle.text = titleNames[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: frame.width / 2 , height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let x = self.frame.width / 2 * CGFloat(indexPath.item)
        
        horizontalLineLeftAnchorConstant?.constant = x
    }
}

class MenuBarCell: BaseCell {
    
    let cellTitle: UILabel = {
        let title = UILabel()
        title.text = "title"
        title.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        title.textAlignment = .center
        title.textColor = UIColor.rgb(red: 220, green: 220, blue: 220)
        return title
    }()
    
    override var isSelected: Bool {
        didSet {
            cellTitle.textColor = isSelected ? UIColor.white : UIColor.rgb(red: 220, green: 220, blue: 220)
        }
    }
    
    override func setupViews() {
        
        addSubview(cellTitle)
        addConstraints(withFormat: "H:[v0]", views: cellTitle)
        addConstraints(withFormat: "V:[v0]", views: cellTitle)
        
        addConstraint(NSLayoutConstraint(item: cellTitle, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: cellTitle, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
    }
}
