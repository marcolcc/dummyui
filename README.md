# dummyUI
### This is my submission for applying to a graduate program at Mindera
In this xcode project you will find my implementation of a dummy user interface for iOS devices that conforms to the wireframe shown in this [video](https://tinyurl.com/mindera-graduates-app).

This code was written using the latest swift platform (4.1)

In this app you can

  * scroll through vertical and horizontal collections
  * navigate between different kinds of UI

Todos

  * improve layout appearance
  * implement the menu feature
